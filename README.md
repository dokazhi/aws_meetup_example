# EKS + TERRAFORM
## Basic example
В репозитории декларативное описание инфрастуктуры в которой есть
* VPC
* EKS
* и еще много мелких компонентов

Чтобы инфраструктура работала нормально, необходимо в файле ```providers``` заполнить переменные 

```ACCESS_KEY``` и ```SECRET_KEY```

### Основные команды
```bash
$ terraform init
$ terraform plan -out plans/some_name.tfplan
$ terraform apply plans/some_name.tfplan
```