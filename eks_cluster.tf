provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}


# Модуль для создания EKS
module "eks" {
  source = "terraform-aws-modules/eks/aws"
  cluster_name    = local.cluster_name
  cluster_version = "1.21"
  subnets         = module.vpc.private_subnets

  tags = {
    Environment = "demo"
  }

  vpc_id = module.vpc.vpc_id

    worker_groups_launch_template  = [
    {
      name = "Spoted-ec2s"
      override_instance_types   = [
        "t3.small",
        "t3.medium"
      ]
      spot_instance_pools       = 4
      asg_max_size              = 8
      asg_desired_capacity      = 4
      kubelet_extra_args        = "--node-labels=node.kubernetes.io/lifecycle=spot"
      public_ip                 = false
      tags = [
        {
          key                 = "k8s.io/cluster-autoscaler/enabled"
          propagate_at_launch = "false"
          value               = "true"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/${local.cluster_name}"
          propagate_at_launch = "false"
          value               = "true"
        }
      ]
    },
  ]
  map_users                            = var.map_users
}