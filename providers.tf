# Здесь нужно указать ваши креденшелсы от aws
provider "aws" {
  access_key = "YOUR_ACCESS_KEY"
  secret_key = "YOUR_SECRET_KEY"
  region  = "eu-north-1" # На момент создания является одним из самый дешевых и близких
}
