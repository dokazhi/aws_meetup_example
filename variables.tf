variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "some_account_id",
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn = string
    username = string
    groups = list(string)
  }))

  default = [
    {
      userarn = "arn:aws:iam::some_account_id:root"
      username = "root"
      groups = [
        "system:masters"]
    },
  ]
}
