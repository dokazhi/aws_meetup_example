module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "DEMO-VPC"
  cidr = "10.0.0.0/16"

  azs             = data.aws_availability_zones.azs.names
  private_subnets      = [for i in range(1, 4): cidrsubnet("10.0.0.0/16", 8, 10 * i + 1)]
  public_subnets       = [for i in range(1 ,4): cidrsubnet("10.0.0.0/16", 8, 10 * i + 2)]
  intra_subnets        = [for i in range(1, 4): cidrsubnet("10.0.0.0/16", 8, 10 * i + 3)]

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

  tags = {
    Terraform = "true"
    Environment = "demo"
  }

    public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}